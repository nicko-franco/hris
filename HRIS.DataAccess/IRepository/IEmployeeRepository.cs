﻿using HRIS.Common.Constants;
using HRIS.Domain.Entities;
using System.Collections.Generic;

namespace HRIS.DataAccess.IRepository
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetAll();
        Employee Get(string property, string value);
        ResponseCode Create(Employee entity);
        ResponseCode Update(Employee entity);
        ResponseCode Delete(string id);
        bool Exists(string property, string value);
    }
}