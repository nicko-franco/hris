﻿using System.Collections.Generic;
using HRIS.Common.Constants;
using HRIS.Domain.Entities;

namespace HRIS.DataAccess.IRepository
{
    public interface IProjectRepository
    {
        IEnumerable<Project> GetAll();
        ResponseCode Create(Project entity);
        ResponseCode Update(Project entity);
        ResponseCode Delete(string id);
        bool Exists(string property, string value);
    }
}