﻿namespace HRIS.DataAccess.IRepository
{
    public interface IAccountRepository
    {
        bool ValidCredentials(string username, string password);
        bool ValidUsername(string username);
    }
}