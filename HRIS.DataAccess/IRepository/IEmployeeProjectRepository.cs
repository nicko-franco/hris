﻿using System.Collections.Generic;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Domain.Entities;

namespace HRIS.DataAccess.IRepository
{
    public interface IEmployeeProjectRepository
    {
        EmployeeProject Get(string employeeId);
        IEnumerable<EmployeeProjectDto> GetAll();
        ResponseCode Create(EmployeeProject entity);
        ResponseCode Update(EmployeeProject entity);
        ResponseCode Delete(string id);
        bool Exists(string property, string value);
    }
}