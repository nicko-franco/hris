﻿using System.Collections.Generic;
using HRIS.Common.Constants;
using HRIS.Domain.Entities;

namespace HRIS.DataAccess.IRepository
{
    public interface IDepartmentRepository
    {
        IEnumerable<Department> GetAll();
        ResponseCode Create(Department entity);
        ResponseCode Update(Department entity);
        ResponseCode Delete(string id);
        bool Exists(string property, string value);
    }
}