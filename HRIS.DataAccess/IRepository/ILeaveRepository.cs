﻿using System.Collections.Generic;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Domain.Entities;

namespace HRIS.DataAccess.IRepository
{
    public interface ILeaveRepository
    {
        LeaveBalanceDto GetBalance(LeaveDto dto);
        IEnumerable<Leave> GetBalances(string employeeNumber);
        ResponseCode Create(Leave entity);
    }
}