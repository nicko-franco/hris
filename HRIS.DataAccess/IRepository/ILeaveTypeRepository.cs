﻿using System.Collections.Generic;
using HRIS.Common.Constants;
using HRIS.Domain.Entities;

namespace HRIS.DataAccess.IRepository
{
    public interface ILeaveTypeRepository
    {
        LeaveType Get(string property, string value);
        IEnumerable<LeaveType> GetAll();
        ResponseCode Create(LeaveType entity);
        ResponseCode Update(LeaveType entity);
        ResponseCode Delete(string id);
        bool Exists(string property, string value);
    }
}