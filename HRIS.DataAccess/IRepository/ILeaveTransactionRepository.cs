﻿using System.Collections.Generic;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Domain.Entities;

namespace HRIS.DataAccess.IRepository
{
    public interface ILeaveTransactionRepository
    {
        IEnumerable<LeaveTransaction> GetAll();
        IEnumerable<LeaveTransactionDto> Get(string employeeNumber);
        ResponseCode Create(LeaveTransaction entity);
    }
}