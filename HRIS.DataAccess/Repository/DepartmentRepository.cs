﻿using HRIS.Common.Constants;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class DepartmentRepository : BaseRepository, IDepartmentRepository
    {
        public DepartmentRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<Department> GetAll()
            => _context.Department
                       .AsNoTracking()
                       .OrderBy(x => x.Name)
                       .ToList();

        public ResponseCode Create(Department entity)
        {
            _context.Department.Add(entity);

            return _context.Save();
        }

        public ResponseCode Update(Department entity)
        {
            _context.Department.Update(entity);

            return _context.Save();
        }

        public ResponseCode Delete(string id)
        {
            _context.Department.Remove(new Department { Id = id });

            return _context.Save();
        }

        public bool Exists(string property, string value)
            => _context.Department.Where(x => x.GetType().GetProperty(property).GetValue(x, null).Equals(value))
                       .AsNoTracking()
                       .FirstOrDefault() != null;
    }
}
