﻿using HRIS.Common.Constants;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class LeaveTypeRepository : BaseRepository, ILeaveTypeRepository
    {
        public LeaveTypeRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<LeaveType> GetAll()
            => _context.LeaveType
                       .AsNoTracking()
                       .OrderBy(x => x.Name)
                       .ToList();

        public LeaveType Get(string property, string value)
            => _context.LeaveType.Where(x => x.GetType().GetProperty(property).GetValue(x, null).Equals(value))
                       .AsNoTracking()
                       .FirstOrDefault();

        public ResponseCode Create(LeaveType entity)
        {
            _context.LeaveType.Add(entity);

            return _context.Save();
        }

        public ResponseCode Update(LeaveType entity)
        {
            _context.LeaveType.Update(entity);

            return _context.Save();
        }

        public ResponseCode Delete(string id)
        {
            _context.LeaveType.Remove(new LeaveType { Id = id });

            return _context.Save();
        }

        public bool Exists(string property, string value)
            => Get(property, value) != null;
    }
}
