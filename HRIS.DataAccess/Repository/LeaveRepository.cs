﻿using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class LeaveRepository : BaseRepository, ILeaveRepository
    {
        public LeaveRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<Leave> GetBalances(string employeeNumber) 
            => _context.Leave
                       .AsNoTracking()
                       .Where(x => x.Employee.Number == employeeNumber)
                       .ToList();

        public LeaveBalanceDto GetBalance(LeaveDto dto)
            => (from leave in _context.Leave
                join type in _context.LeaveType
                on leave.LeaveTypeId equals type.Id
                where leave.Employee.Number == dto.EmployeeNumber && leave.LeaveTypeId == dto.LeaveTypeId && leave.Year == dto.Year
                select new LeaveBalanceDto
                {
                    Year = leave.Year,
                    Balance = leave.Balance,
                    LeaveType = type.Name
                }).FirstOrDefault();

        public ResponseCode Create(Leave entity)
        {
            _context.Leave.Add(entity);

            return _context.Save();
        }
    }
}
