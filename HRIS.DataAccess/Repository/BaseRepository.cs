﻿using HRIS.Persistence.Interfaces;

namespace HRIS.DataAccess.Repository
{
    public class BaseRepository
    {
        protected readonly IAppDbContext _context;

        protected BaseRepository(IAppDbContext context)
            => _context = context;
    }
}
