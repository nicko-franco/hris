﻿using HRIS.Common.Constants;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class ProjectRepository : BaseRepository, IProjectRepository
    {
        public ProjectRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<Project> GetAll()
            => _context.Project
                       .AsNoTracking()
                       .OrderBy(x => x.Name)
                       .ToList();

        public ResponseCode Create(Project entity)
        {
            _context.Project.Add(entity);

            return _context.Save();
        }

        public ResponseCode Update(Project entity)
        {
            _context.Project.Update(entity);

            return _context.Save();
        }

        public ResponseCode Delete(string id)
        {
            _context.Project.Remove(new Project { Id = id });

            return _context.Save();
        }

        public bool Exists(string property, string value)
            => _context.Project.Where(x => x.GetType().GetProperty(property).GetValue(x, null).Equals(value))
                       .AsNoTracking()
                       .FirstOrDefault() != null;
    }
}
