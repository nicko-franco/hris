﻿using HRIS.Common.Constants;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class EmployeeRepository : BaseRepository, IEmployeeRepository
    {
        public EmployeeRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<Employee> GetAll()
            => _context.Employee
                       .AsNoTracking()
                       .OrderBy(x => x.Number)
                       .ToList();

        public Employee Get(string property, string value)
            => _context.Employee.Where(x => x.GetType().GetProperty(property).GetValue(x, null).Equals(value))
                       .AsNoTracking()
                       .FirstOrDefault();

        public ResponseCode Create(Employee entity)
        {
            _context.Employee.Add(entity);

            return _context.Save();
        }

        public ResponseCode Update(Employee entity)
        {
            return _context.Save();
        }

        public ResponseCode Delete(string id)
        {
            _context.Employee.Remove(new Employee { Id = id });

            return _context.Save();
        }

        public bool Exists(string property, string value)
            => _context.Employee.Where(x => x.GetType().GetProperty(property).GetValue(x, null).Equals(value))
                       .AsNoTracking()
                       .FirstOrDefault() != null;
    }
}