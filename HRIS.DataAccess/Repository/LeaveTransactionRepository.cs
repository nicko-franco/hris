﻿using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class LeaveTransactionRepository : BaseRepository, ILeaveTransactionRepository
    {
        public LeaveTransactionRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<LeaveTransaction> GetAll()
            => _context.LeaveTransaction
                       .AsNoTracking()
                       .OrderBy(x => x.Employee.Id)
                       .OrderByDescending(x => x.AppliedDate)
                       .ToList();

        public IEnumerable<LeaveTransactionDto> Get(string employeeNumber)
        {
            var leaveTransactions =
               (from leave in _context.LeaveTransaction
                join type in _context.LeaveType
                on leave.LeaveTypeId equals type.Id
                join emp in _context.Employee
                on leave.EmployeeId equals emp.Id
                where emp.Number == employeeNumber
                select new LeaveTransactionDto
                {
                    Id = leave.Id,
                    EmployeeId = emp.Id,
                    EmployeeNumber = emp.Number,
                    LastName = emp.LastName,
                    FirstName = emp.FirstName,
                    LeaveType = type.Name,
                    LeaveTypeId = type.Id,
                    Status = leave.Status,
                    Reason = leave.Reason,
                    DeclinedReason = leave.DeclinedReason,
                    Amount = leave.Amount,
                    FromDate = leave.FromDate,
                    ToDate = leave.ToDate,
                    AppliedDate = leave.AppliedDate,
                    ApprovedDate = leave.ApprovedDate
                }).ToList();

            return leaveTransactions;
        }

        public ResponseCode Create(LeaveTransaction entity)
        {
            _context.LeaveTransaction.Add(entity);

            return _context.Save();
        }
    }
}
