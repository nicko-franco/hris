﻿using HRIS.DataAccess.IRepository;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        public AccountRepository(IAppDbContext context) : base(context) { }

        public bool ValidUsername(string username)
            => _context.Employee.Where(x => x.Number == username).AsNoTracking().FirstOrDefault() != null;

        public bool ValidCredentials(string username, string password)
            => _context.Employee.Where(x => x.Number == username && x.Password == password).AsNoTracking().FirstOrDefault() != null;
    }
}