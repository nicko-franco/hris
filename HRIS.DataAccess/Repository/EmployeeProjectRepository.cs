﻿using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HRIS.DataAccess.Repository
{
    public class EmployeeProjectRepository : BaseRepository, IEmployeeProjectRepository
    {
        public EmployeeProjectRepository(IAppDbContext context) : base(context) { }

        public IEnumerable<EmployeeProjectDto> GetAll()
        {
            var employeeProjects =
                (from employeeProject in _context.EmployeeProject
                 join employee in _context.Employee
                 on employeeProject.EmployeeId equals employee.Id
                 join project in _context.Project
                 on employeeProject.ProjectId equals project.Id
                 select new EmployeeProjectDto
                 {
                     Id = employeeProject.Id,
                     EmployeeId = employeeProject.EmployeeId,
                     EmployeeNumber = employee.Number,
                     FirstName = employee.FirstName,
                     LastName = employee.LastName,
                     ProjectId = employeeProject.ProjectId,
                     ProjectName = project.Name
                 })
                 .AsNoTracking()
                 .ToList();

            return employeeProjects;
        }

        public EmployeeProject Get(string employeeId)
            => _context.EmployeeProject.Where(x => x.EmployeeId == employeeId)
                       .AsNoTracking()
                       .FirstOrDefault();

        public ResponseCode Create(EmployeeProject entity)
        {
            _context.EmployeeProject.Add(entity);

            return _context.Save();
        }

        public ResponseCode Update(EmployeeProject entity)
        {
            _context.EmployeeProject.Update(entity);

            return _context.Save();
        }

        public ResponseCode Delete(string id)
        {
            _context.EmployeeProject.Remove(new EmployeeProject { Id = id });

            return _context.Save();
        }

        public bool Exists(string property, string value)
            => _context.EmployeeProject.Where(x => x.GetType().GetProperty(property).GetValue(x, null).Equals(value))
                       .AsNoTracking()
                       .FirstOrDefault() != null;
    }
}
