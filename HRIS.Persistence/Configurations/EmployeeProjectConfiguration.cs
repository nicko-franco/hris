﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class EmployeeProjectConfiguration : IEntityTypeConfiguration<EmployeeProject>
    {
        public void Configure(EntityTypeBuilder<EmployeeProject> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                   .HasDefaultValue(Guid.NewGuid().ToString())
                   .HasMaxLength(40);

            builder.HasOne(x => x.Employee).WithMany().HasForeignKey(x => x.EmployeeId);

            builder.HasOne(x => x.Project).WithMany().HasForeignKey(x => x.ProjectId);

            builder.Property(x => x.EmployeeId)
                   .IsRequired()
                   .HasMaxLength(40);

            builder.Property(x => x.ProjectId)
                   .IsRequired()
                   .HasMaxLength(40);
        }
    }
}
