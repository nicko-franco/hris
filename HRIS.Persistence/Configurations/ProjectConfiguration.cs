﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id)
                .HasDefaultValue(Guid.NewGuid().ToString())
                .HasMaxLength(40);

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(150);
        }
    }
}
