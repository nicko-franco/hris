﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class LeaveTransactionConfiguration : IEntityTypeConfiguration<LeaveTransaction>
    {
        public void Configure(EntityTypeBuilder<LeaveTransaction> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                   .HasDefaultValue(Guid.NewGuid().ToString())
                   .HasMaxLength(40);

            builder.HasOne(x => x.Employee).WithMany().HasForeignKey(x => x.EmployeeId);

            builder.HasOne(x => x.LeaveType).WithMany().HasForeignKey(x => x.LeaveTypeId);

            builder.Property(x => x.Amount)
                   .IsRequired()
                   .HasDefaultValue(0);

            builder.Property(x => x.FromDate)
                   .IsRequired()
                   .HasDefaultValue(DateTime.Today);

            builder.Property(x => x.ToDate)
                   .IsRequired()
                   .HasDefaultValue(DateTime.Today);

            builder.Property(x => x.AppliedDate)
                   .IsRequired()
                   .HasDefaultValue(DateTime.Today);

            builder.Property(x => x.Status)
                   .HasMaxLength(20);

            builder.Property(x => x.ApprovedDate);

            builder.Property(x => x.Reason)
                   .HasMaxLength(200);

            builder.Property(x => x.DeclinedReason)
                   .HasMaxLength(200);
        }
    }
}
