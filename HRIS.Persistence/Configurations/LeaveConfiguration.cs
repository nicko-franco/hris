﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class LeaveConfiguration : IEntityTypeConfiguration<Leave>
    {
        public void Configure(EntityTypeBuilder<Leave> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                   .HasDefaultValue(Guid.NewGuid().ToString())
                   .HasMaxLength(40);

            builder.Property(x => x.LeaveTypeId)
                   .IsRequired()
                   .HasMaxLength(40);

            builder.HasOne(x => x.Employee).WithMany().HasForeignKey(x => x.EmployeeId);

            builder.HasOne(x => x.LeaveType).WithMany().HasForeignKey(x => x.LeaveTypeId);

            builder.Property(x => x.Balance)
                   .IsRequired()
                   .HasDefaultValue(0);

            builder.Property(x => x.Year)
                   .IsRequired()
                   .HasDefaultValue(DateTime.Now.Year);
        }
    }
}