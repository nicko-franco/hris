﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class EmployeeSuperiorConfiguration : IEntityTypeConfiguration<EmployeeSuperior>
    {
        public void Configure(EntityTypeBuilder<EmployeeSuperior> builder)
        {
            builder.HasKey(es => es.Id);
            builder.Property(es => es.Id)
                   .HasDefaultValue(Guid.NewGuid().ToString())
                   .HasMaxLength(40);

            builder.HasOne(es => es.Superior)
                   .WithOne()
                   .HasForeignKey<Employee>(e => e.Id);
        }
    }
}
