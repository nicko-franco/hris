﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                   .HasDefaultValue(Guid.NewGuid().ToString())
                   .HasMaxLength(40);

            builder.Property(e => e.Number)
                   .IsRequired()
                   .HasMaxLength(20);

            builder.Property(e => e.FirstName)
                   .IsRequired()
                   .HasMaxLength(50);

            builder.Property(e => e.LastName)
                   .IsRequired()
                   .HasMaxLength(50);

            builder.Property(e => e.MiddleName)
                   .HasMaxLength(50);

            builder.Property(e => e.DepartmentId)
                   .HasMaxLength(40);

            builder.HasOne(e => e.Department)
                   .WithOne()
                   .HasForeignKey<Department>(d => d.Id);
        }
    }
}
