﻿using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace HRIS.Persistence.Configurations
{
    public class LeaveTypeConfiguration : IEntityTypeConfiguration<LeaveType>
    {
        public void Configure(EntityTypeBuilder<LeaveType> builder)
        {
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Id)
                   .HasDefaultValue(Guid.NewGuid().ToString())
                   .HasMaxLength(40);

            builder.Property(t => t.Name)
                   .IsRequired()
                   .HasMaxLength(40);
        }
    }
}
