﻿using HRIS.Common.Constants;
using HRIS.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Persistence.Interfaces
{
    public interface IAppDbContext
    {
        DbSet<Department> Department { get; set; }

        DbSet<Project> Project { get; set; }

        DbSet<Employee> Employee { get; set; }

        DbSet<EmployeeProject> EmployeeProject { get; set; }

        DbSet<EmployeeSuperior> EmployeeSuperior { get; set; }

        DbSet<Leave> Leave { get; set; }

        DbSet<LeaveType> LeaveType { get; set; }

        DbSet<LeaveTransaction> LeaveTransaction { get; set; }

        ResponseCode Save();
    }
}
