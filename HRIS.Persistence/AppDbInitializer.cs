﻿using HRIS.Domain.Entities;
using System.Collections.Generic;

namespace HRIS.Persistence
{
    public class AppDbInitializer
    {
        private readonly Dictionary<int, Employee> Employees = new Dictionary<int, Employee>();

        public static void Initialize(AppDbContext context)
        {
            var initializer = new AppDbInitializer();
            initializer.SeedEverything(context);
        }

        public void SeedEverything(AppDbContext context)
        {
            context.Database.EnsureCreated();

            SeedEmployees(context);
        }

        public void SeedEmployees(AppDbContext context)
        {
            var employees = new[]
            {
                new Employee { Id = "bfde36ac-5c06-4bbf-b3f9-14af494ed8ed", Number = "82590", LastName = "Franco", FirstName = "Nicko", MiddleName = "Lapuz", DepartmentId = "04494d98-dcb3-487b-a514-45aaf90050a1" }
            };

            context.Employee.AddRange(employees);
            context.SaveChanges();
        }
    }
}
