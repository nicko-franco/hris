﻿using HRIS.Common.Constants;
using HRIS.Domain.Entities;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Persistence
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Department> Department { get; set; }

        public DbSet<Project> Project { get; set; }

        public DbSet<Employee> Employee { get; set; }

        public DbSet<EmployeeProject> EmployeeProject { get; set; }

        public DbSet<EmployeeSuperior> EmployeeSuperior { get; set; }

        public DbSet<Leave> Leave { get; set; }

        public DbSet<LeaveType> LeaveType { get; set; }

        public DbSet<LeaveTransaction> LeaveTransaction { get; set; }

        public ResponseCode Save()
        {
            if (SaveChanges() > 0)
                return ResponseCode.Success;
                    
            return ResponseCode.Failed;
        }
    }
}
