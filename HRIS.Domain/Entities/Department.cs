﻿namespace HRIS.Domain.Entities
{
    public class Department
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Head { get; set; }
    }
}
