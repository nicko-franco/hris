﻿namespace HRIS.Domain.Entities
{
    public class Project
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
