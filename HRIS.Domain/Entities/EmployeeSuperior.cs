﻿namespace HRIS.Domain.Entities
{
    public class EmployeeSuperior
    {
        public string Id { get; set; }

        public virtual Employee Staff { get; set; }
        public virtual Employee Superior { get; set; }
    }
}
