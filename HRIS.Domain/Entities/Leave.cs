﻿namespace HRIS.Domain.Entities
{
    public class Leave
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string LeaveTypeId { get; set; }

        public int Year { get; set; }
        public decimal Balance { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual LeaveType LeaveType { get; set; }
    }
}
