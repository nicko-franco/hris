﻿using System;

namespace HRIS.Domain.Entities
{
    public class LeaveTransaction
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string LeaveTypeId { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string DeclinedReason { get; set; }

        public decimal Amount { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime AppliedDate { get; set; }
        public DateTime? ApprovedDate { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual LeaveType LeaveType { get; set; }
    }
}
