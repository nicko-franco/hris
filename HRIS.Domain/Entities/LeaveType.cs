﻿namespace HRIS.Domain.Entities
{
    public class LeaveType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
