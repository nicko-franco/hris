﻿using System.Collections.Generic;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;

namespace HRIS.Application.IServices
{
    public interface ILeaveTypeService
    {
        BaseResponse Create(LeaveTypeDto dto);
        BaseResponse Delete(string id);
        BaseResponse<IEnumerable<LeaveTypeDto>> GetAll();
        BaseResponse Update(LeaveTypeDto dto);
    }
}