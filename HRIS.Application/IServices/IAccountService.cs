﻿using HRIS.Common.Dtos;
using HRIS.Common.Responses;

namespace HRIS.Application.IServices
{
    public interface IAccountService
    {
        BaseResponse<LoginResponse> Login(LoginDto dto);
    }
}