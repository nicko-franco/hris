﻿using System.Collections.Generic;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;

namespace HRIS.Application.IServices
{
    public interface ILeaveTransactionService
    {
        BaseResponse Create(LeaveTransactionDto dto);
        BaseResponse<IEnumerable<LeaveTransactionDto>> Get(string employeeNumber);
    }
}