﻿using System.Collections.Generic;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;

namespace HRIS.Application.IServices
{
    public interface IProjectService
    {
        BaseResponse<IEnumerable<ProjectDto>> GetAll();
        BaseResponse Create(ProjectDto dto);
        BaseResponse Update(ProjectDto dto);
        BaseResponse Delete(string id);
    }
}