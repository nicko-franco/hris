﻿using System.Collections.Generic;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;

namespace HRIS.Application.IServices
{
    public interface ILeaveService
    {
        BaseResponse<LeaveBalanceDto> GetBalance(LeaveDto dto);
        BaseResponse<IEnumerable<LeaveBalanceDto>> GetBalances(string employeeNumber);
        BaseResponse Create(LeaveDto dto);
    }
}