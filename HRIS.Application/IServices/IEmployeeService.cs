﻿using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using System.Collections.Generic;

namespace HRIS.Application.IServices
{
    public interface IEmployeeService
    {
        BaseResponse<IEnumerable<EmployeeDto>> GetAll();
        BaseResponse Create(EmployeeDto dto);
        BaseResponse Update(EmployeeDto dto);
        BaseResponse Delete(string id);
        BaseResponse AssignProject(EmployeeProjectDto dto);
        BaseResponse<IEnumerable<EmployeeProjectDto>> GetEmployeeProjects();
    }
}