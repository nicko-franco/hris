﻿using System.Collections.Generic;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;

namespace HRIS.Application.IServices
{
    public interface IDepartmentService
    {
        BaseResponse Create(DepartmentDto dto);
        BaseResponse Delete(string id);
        BaseResponse<IEnumerable<DepartmentDto>> GetAll();
        BaseResponse Update(DepartmentDto dto);
    }
}