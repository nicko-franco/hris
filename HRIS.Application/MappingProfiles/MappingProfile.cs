﻿using AutoMapper;
using HRIS.Common.Dtos;
using HRIS.Domain.Entities;

namespace HRIS.Application.MappingProfiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmployeeDto, Employee>()
                .ForMember(x => x.Department, opts => opts.Ignore());

            CreateMap<LeaveDto, Leave>()
                .ForMember(x => x.Employee, opts => opts.Ignore())
                .ForMember(x => x.LeaveType, opts => opts.Ignore());

            CreateMap<Leave, LeaveDto>()
                .ForMember(x => x.EmployeeNumber, opts => opts.Ignore());

            CreateMap<LeaveTransactionDto, LeaveTransaction>()
                .ForMember(x => x.Employee, opts => opts.Ignore())
                .ForMember(x => x.LeaveType, opts => opts.Ignore());

            CreateMap<LeaveTransaction, LeaveTransactionDto>()
                .ForMember(x => x.EmployeeNumber, opts => opts.MapFrom(x => x.Employee.Number))
                .ForMember(x => x.FirstName, opts => opts.MapFrom(x => x.Employee.FirstName))
                .ForMember(x => x.LastName, opts => opts.MapFrom(x => x.Employee.LastName))
                .ForMember(x => x.LeaveType, opts => opts.MapFrom(x => x.LeaveType.Name));

            CreateMap<EmployeeProjectDto, EmployeeProject>()
                .ForMember(x => x.Employee, opts => opts.Ignore())
                .ForMember(x => x.Project, opts => opts.Ignore());

            CreateMap<EmployeeProject, EmployeeProjectDto>()
                .ForMember(x => x.FirstName, opts => opts.MapFrom(x => x.Employee.FirstName))
                .ForMember(x => x.LastName, opts => opts.MapFrom(x => x.Employee.LastName))
                .ForMember(x => x.EmployeeNumber, opts => opts.MapFrom(x => x.Employee.Number))
                .ForMember(x => x.ProjectName, opts => opts.MapFrom(x => x.Project.Name));
        }
    }
}
