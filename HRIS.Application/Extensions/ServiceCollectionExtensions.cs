﻿using HRIS.Application.MappingProfiles;
using HRIS.Persistence;
using HRIS.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HRIS.API.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<IAppDbContext, AppDbContext>(options =>
                options.UseMySQL(configuration.GetConnectionString("HrisDatabase")));

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            var interfaces = new List<Type>();

            Assembly.Load("HRIS.DataAccess")
                    .GetTypes()
                    .Where(c => c.IsInterface && c.Name.EndsWith("Repository"))
                    .ToList()
                    .ForEach(repository => interfaces.Add(repository));

            Assembly.Load("HRIS.DataAccess")
                    .GetTypes()
                    .Where(c => c.IsClass && c.Name.EndsWith("Repository") && c.Name != "BaseRepository")
                    .ToList()
                    .ForEach(repository =>
                    {
                        var iRepository = interfaces.FirstOrDefault(x => x.Name.ToString() == $"I{repository.Name}");
                        services.AddTransient(iRepository, repository);
                    });

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var interfaces = new List<Type>();

            Assembly.Load("HRIS.Application")
                    .GetTypes()
                    .Where(c => c.IsInterface && c.Name.EndsWith("Service"))
                    .ToList()
                    .ForEach(service => interfaces.Add(service));

            Assembly.Load("HRIS.Application")
                    .GetTypes()
                    .Where(c => c.IsClass && c.Name.EndsWith("Service") && c.Name != "BaseService")
                    .ToList()
                    .ForEach(service =>
                    {
                        var iService = interfaces.FirstOrDefault(x => x.Name.ToString() == $"I{service.Name}");
                        services.AddTransient(iService, service);
                    });

            return services;
        }

        public static IServiceCollection AddMappingProfile(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<MappingProfile>();

            return services;
        }
    }
}
