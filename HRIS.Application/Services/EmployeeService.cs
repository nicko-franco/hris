﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Extensions;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class EmployeeService : BaseService, IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeProjectRepository _employeeProjectRepository;

        public EmployeeService(IEmployeeRepository employeeRepository, IEmployeeProjectRepository employeeProjectRepository)
        {
            _employeeRepository = employeeRepository;
            _employeeProjectRepository = employeeProjectRepository;
        }

        public BaseResponse<IEnumerable<EmployeeDto>> GetAll()
            => Response<IEnumerable<EmployeeDto>>(ResponseCode.Success,
                _employeeRepository.GetAll().Select(x => Map<EmployeeDto>(x)).ToList());

        public BaseResponse Create(EmployeeDto dto)
        {
            if (_employeeRepository.Exists("Number", dto.Number))
                return Response(ResponseCode.DuplicateRecord);

            dto.Password = dto.Password.Hash(Cryptography.Encrypt);

            return Response(_employeeRepository.Create(Map<Employee>(dto)));
        }

        public BaseResponse Update(EmployeeDto dto)
        {
            if (!_employeeRepository.Exists("Id", dto.Id))
                return Response(ResponseCode.RecordNotFound);

            return Response(_employeeRepository.Update(Map<Employee>(dto)));
        }

        public BaseResponse Delete(string id)
        {
            if (!_employeeRepository.Exists("Id", id))
                return Response(ResponseCode.RecordNotFound);

            return Response(_employeeRepository.Delete(id));
        }

        #region Employee Project
        public BaseResponse<IEnumerable<EmployeeProjectDto>> GetEmployeeProjects()
            => Response<IEnumerable<EmployeeProjectDto>>(ResponseCode.Success,
                _employeeProjectRepository.GetAll().Select(x => Map<EmployeeProjectDto>(x)).ToList());

        public BaseResponse AssignProject(EmployeeProjectDto dto)
        {
            if (!_employeeRepository.Exists("Id", dto.EmployeeId))
                return Response(ResponseCode.RecordNotFound);

            var assignedProject = _employeeProjectRepository.Get(dto.EmployeeId);
            if (assignedProject != null)
            {
                dto.Id = assignedProject.Id;
                return Response(_employeeProjectRepository.Update(Map<EmployeeProject>(dto)));
            }

            return Response(_employeeProjectRepository.Create(Map<EmployeeProject>(dto)));
        }
        #endregion Employee Project
    }
}
