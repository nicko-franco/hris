﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Extensions;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class LeaveTransactionService : BaseService, ILeaveTransactionService
    {
        private readonly ILeaveTransactionRepository _leaveTransactionRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public LeaveTransactionService(
            ILeaveTransactionRepository leaveTransactionRepository,
            IEmployeeRepository employeeRepository)
        {
            _leaveTransactionRepository = leaveTransactionRepository;
            _employeeRepository = employeeRepository;
        }

        public BaseResponse<IEnumerable<LeaveTransactionDto>> Get(string employeeNumber)
        {
            if (!_employeeRepository.Exists("Number", employeeNumber))
                return Response(ResponseCode.RecordNotFound, new List<LeaveTransactionDto>().AsEnumerable(), string.Format(GenericError.RecordNotFound.Description(), "Employee record"));

            var list = _leaveTransactionRepository.Get(employeeNumber);
            return Response(ResponseCode.Success, list);
        }

        public BaseResponse Create(LeaveTransactionDto dto)
        {
            if (!_employeeRepository.Exists("Id", dto.EmployeeId))
                return Response(ResponseCode.RecordNotFound, string.Format(GenericError.RecordNotFound.Description(), "Employee record"));

            return Response(_leaveTransactionRepository.Create(Map<LeaveTransaction>(dto)));
        }
    }
}
