﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IProjectRepository _repository;

        public ProjectService(IProjectRepository repository)
            => _repository = repository;

        public BaseResponse<IEnumerable<ProjectDto>> GetAll()
            => Response<IEnumerable<ProjectDto>>(ResponseCode.Success,
                _repository.GetAll().Select(x => Map<ProjectDto>(x)).ToList());

        public BaseResponse Create(ProjectDto dto)
        {
            if (_repository.Exists("Name", dto.Name))
                return Response(ResponseCode.DuplicateRecord);

            return Response(_repository.Create(Map<Project>(dto)));
        }

        public BaseResponse Update(ProjectDto dto)
        {
            if (!_repository.Exists("Id", dto.Id))
                return Response(ResponseCode.RecordNotFound);

            return Response(_repository.Update(Map<Project>(dto)));
        }

        public BaseResponse Delete(string id)
        {
            if (!_repository.Exists("Id", id))
                return Response(ResponseCode.RecordNotFound);

            return Response(_repository.Delete(id));
        }
    }
}
