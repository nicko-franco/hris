﻿using HRIS.Common.Constants;
using HRIS.Common.Responses;

namespace HRIS.Application.Services
{
    public class BaseService
    {
        protected BaseResponse Response(ResponseCode code, string message = null)
            => new BaseResponse { Code = code, Message = message };

        protected BaseResponse<T> Response<T>(ResponseCode code, T data, string message = null)
            => new BaseResponse<T> { Code = code, Data = data, Message = message };
    }
}