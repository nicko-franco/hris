﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Extensions;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class AccountService : BaseService, IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public AccountService(IAccountRepository accountRepository, IEmployeeRepository employeeRepository)
        {
            _accountRepository = accountRepository;
            _employeeRepository = employeeRepository;
        }

        public BaseResponse<LoginResponse> Login(LoginDto dto)
        {
            dto.Password = dto.Password.Hash(Cryptography.Encrypt);

            if (!_accountRepository.ValidUsername(dto.Username))
                return Response(ResponseCode.Failed, new LoginResponse(), AuthenticationMessage.UsernameDoesNotExists.Description());

            if (!_accountRepository.ValidCredentials(dto.Username, dto.Password))
                return Response(ResponseCode.Failed, new LoginResponse(), AuthenticationMessage.InvalidPassword.Description());

            return Response(ResponseCode.Success, Map<LoginResponse>(_employeeRepository.Get("Number", dto.Username)));
        }
    }
}
