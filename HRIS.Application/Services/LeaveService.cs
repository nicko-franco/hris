﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Extensions;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class LeaveService : BaseService, ILeaveService
    {
        private readonly ILeaveRepository _leaveRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public LeaveService(ILeaveRepository leaveRepository, IEmployeeRepository employeeRepository)
        {
            _leaveRepository = leaveRepository;
            _employeeRepository = employeeRepository;
        }

        public BaseResponse<LeaveBalanceDto> GetBalance(LeaveDto dto)
        {
            if (!_employeeRepository.Exists("Number", dto.EmployeeNumber))
                return Response(ResponseCode.RecordNotFound, new LeaveBalanceDto(), string.Format(GenericError.RecordNotFound.Description(), "Employee record"));

            return Response(ResponseCode.Success, _leaveRepository.GetBalance(dto));
        }

        public BaseResponse<IEnumerable<LeaveBalanceDto>> GetBalances(string employeeNumber)
        {
            if (!_employeeRepository.Exists("Number", employeeNumber))
                return Response(ResponseCode.RecordNotFound, new List<LeaveBalanceDto>().AsEnumerable(), string.Format(GenericError.RecordNotFound.Description(), "Employee record"));

            return Response(ResponseCode.Success, _leaveRepository.GetBalances(employeeNumber).Select(x => Map<LeaveBalanceDto>(x)));
        }

        public BaseResponse Create(LeaveDto dto)
            => Response(_leaveRepository.Create(Map<Leave>(dto)));
    }
}
