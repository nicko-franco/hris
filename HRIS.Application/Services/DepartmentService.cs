﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class DepartmentService : BaseService, IDepartmentService
    {
        private readonly IDepartmentRepository _repository;

        public DepartmentService(IDepartmentRepository repository)
            => _repository = repository;

        public BaseResponse<IEnumerable<DepartmentDto>> GetAll()
            => Response<IEnumerable<DepartmentDto>>(ResponseCode.Success,
                _repository.GetAll().Select(x => Map<DepartmentDto>(x)).ToList());

        public BaseResponse Create(DepartmentDto dto)
        {
            if (_repository.Exists("Name", dto.Name))
                return Response(ResponseCode.DuplicateRecord);

            return Response(_repository.Create(Map<Department>(dto)));
        }

        public BaseResponse Update(DepartmentDto dto)
        {
            if (!_repository.Exists("Id", dto.Id))
                return Response(ResponseCode.RecordNotFound);

            return Response(_repository.Update(Map<Department>(dto)));
        }

        public BaseResponse Delete(string id)
        {
            if (!_repository.Exists("Id", id))
                return Response(ResponseCode.RecordNotFound);

            return Response(_repository.Delete(id));
        }
    }
}
