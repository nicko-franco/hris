﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Extensions;
using HRIS.Common.Responses;
using HRIS.DataAccess.IRepository;
using HRIS.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using static AutoMapper.Mapper;

namespace HRIS.Application.Services
{
    public class LeaveTypeService : BaseService, ILeaveTypeService
    {
        private readonly ILeaveTypeRepository _repository;

        public LeaveTypeService(ILeaveTypeRepository repository)
            => _repository = repository;

        public BaseResponse<IEnumerable<LeaveTypeDto>> GetAll()
            => Response(ResponseCode.Success, _repository.GetAll().Select(x => Map<LeaveTypeDto>(x)));

        public BaseResponse Create(LeaveTypeDto dto)
        {
            if (_repository.Exists("Name", dto.Name))
                return Response(ResponseCode.DuplicateRecord, string.Format(GenericError.DuplicateRecord.Description(), "Leave Type"));

            return Response(_repository.Create(Map<LeaveType>(dto)));
        }

        public BaseResponse Update(LeaveTypeDto dto)
        {
            if (!_repository.Exists("Id", dto.Id))
                return Response(ResponseCode.RecordNotFound, string.Format(GenericError.RecordNotFound.Description(), "Leave Type"));

            return Response(_repository.Update(Map<LeaveType>(dto)));
        }

        public BaseResponse Delete(string id)
        {
            if (!_repository.Exists("Id", id))
                return Response(ResponseCode.RecordNotFound, string.Format(GenericError.RecordNotFound.Description(), "Leave Type"));

            return Response(_repository.Delete(id));
        }
    }
}
