﻿using System.Collections.Generic;
using HRIS.Application.IServices;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HRIS.API.Controllers
{
    [Route("api/project")]
    [ApiController]
    [Authorize]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet("list")]
        public BaseResponse<IEnumerable<ProjectDto>> GetAll()
            => _projectService.GetAll();

        [HttpPost("create")]
        public BaseResponse Create([FromBody] ProjectDto dto)
            => _projectService.Create(dto);

        [HttpPut("update")]
        public BaseResponse Update([FromBody] ProjectDto dto)
            => _projectService.Update(dto);

        [HttpDelete("delete")]
        public BaseResponse Delete([FromBody] string id)
            => _projectService.Delete(id);
    }
}