﻿using System.Net;
using HRIS.API.Errors;
using Microsoft.AspNetCore.Mvc;

namespace HRIS.API.Controllers
{
    [Route("api/errors")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        [Route("{code}")]
        protected IActionResult Error(int code) 
            => new ObjectResult(new ApiError(code, ((HttpStatusCode)code).ToString()));
    }
}