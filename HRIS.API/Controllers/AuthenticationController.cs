﻿using HRIS.Application.IServices;
using HRIS.Common.Constants;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HRIS.API.Controllers
{
    [Route("api/authentication")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AuthenticationController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<BaseResponse<LoginResponse>> Login([FromBody] LoginDto dto)
        {
            var response = _accountService.Login(dto);

            if (response.Code == ResponseCode.Success)
                await Authenticate(response.Data);
            else
                return new BaseResponse<LoginResponse> { Code = response.Code, Data = null, Message = response.Message };

            return response;
        }

        [HttpPost("logout")]
        public async Task Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        private async Task Authenticate(LoginResponse employee)
        {
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);

            identity.AddClaims(new[]
            {
                new Claim(ClaimTypes.Sid, employee.Number),
                new Claim(ClaimTypes.GivenName, employee.FirstName),
                new Claim(ClaimTypes.Surname, employee.LastName)
            });

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
        }
    }
}