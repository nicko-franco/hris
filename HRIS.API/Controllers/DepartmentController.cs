﻿using System.Collections.Generic;
using HRIS.Application.IServices;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HRIS.API.Controllers
{
    [Route("api/department")]
    [ApiController]
    [Authorize]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;
        
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [HttpGet("list")]
        public BaseResponse<IEnumerable<DepartmentDto>> GetAll()
            => _departmentService.GetAll();

        [HttpPost("create")]
        public BaseResponse Create([FromBody] DepartmentDto dto)
            => _departmentService.Create(dto);

        [HttpPut("update")]
        public BaseResponse Update([FromBody] DepartmentDto dto)
            => _departmentService.Update(dto);

        [HttpDelete("delete")]
        public BaseResponse Delete([FromBody] string id)
            => _departmentService.Delete(id);
    }
}