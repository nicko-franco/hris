﻿using HRIS.Application.IServices;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HRIS.API.Controllers
{
    [Route("api/employee")]
    [ApiController]
    [Authorize]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
            => _employeeService = employeeService;

        [HttpGet("list")]
        public BaseResponse<IEnumerable<EmployeeDto>> GetAll()
            => _employeeService.GetAll();

        [HttpPost("create")]
        public BaseResponse Create([FromBody] EmployeeDto dto)
            => _employeeService.Create(dto);

        [HttpPut("update")]
        public BaseResponse Update([FromBody] EmployeeDto dto)
            => _employeeService.Update(dto);

        [HttpDelete("delete")]
        public BaseResponse Delete([FromBody] string id)
            => _employeeService.Delete(id);

        [HttpGet("projects")]
        public BaseResponse<IEnumerable<EmployeeProjectDto>> GetEmployeeProjects()
            => _employeeService.GetEmployeeProjects();

        [HttpPost("assignproject")]
        public BaseResponse AssignProject([FromBody] EmployeeProjectDto dto)
            => _employeeService.AssignProject(dto);
    }
}