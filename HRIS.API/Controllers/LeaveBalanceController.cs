﻿using System.Collections.Generic;
using HRIS.Application.IServices;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HRIS.API.Controllers
{
    [Route("api/leave/balance")]
    [ApiController]
    [Authorize]
    public class LeaveBalanceController : ControllerBase
    {
        private readonly ILeaveService _leaveService;

        public LeaveBalanceController(ILeaveService leaveService)
        {
            _leaveService = leaveService;
        }

        [HttpGet("{employeeNumber}")]
        public BaseResponse<IEnumerable<LeaveBalanceDto>> GetBalances([FromRoute] string employeeNumber)
            => _leaveService.GetBalances(employeeNumber);

        [HttpGet("{employeeNumber}/{year}/{type}")]
        public BaseResponse<LeaveBalanceDto> GetBalance([FromRoute] string employeeNumber, [FromRoute] int year, [FromRoute] string type)
            => _leaveService.GetBalance(new LeaveDto { EmployeeNumber = employeeNumber, Year = year, LeaveTypeId = type });

        [HttpPost("create")]
        public BaseResponse Create([FromBody] LeaveDto dto)
            => _leaveService.Create(dto);
    }
}