﻿using Microsoft.AspNetCore.Mvc;

namespace HRIS.API.Controllers
{
    [Route("api")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DefaultController : ControllerBase
    {
        [HttpGet("version")]
        public string Version()
        {
            return "HRIS WEB API version 1.0";
        }
    }
}