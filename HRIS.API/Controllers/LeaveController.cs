﻿using HRIS.Application.IServices;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HRIS.API.Controllers
{
    [Route("api/leave")]
    [ApiController]
    public class LeaveController : ControllerBase
    {
        private readonly ILeaveTransactionService _leaveTransactionService;

        public LeaveController(ILeaveTransactionService leaveTransactionService)
        {
            _leaveTransactionService = leaveTransactionService;
        }

        [HttpGet("transactions/{employeeNumber}")]
        public BaseResponse<IEnumerable<LeaveTransactionDto>> LeaveTransactions([FromRoute] string employeeNumber)
            => _leaveTransactionService.Get(employeeNumber);

        [HttpPost("apply")]
        public BaseResponse Apply([FromBody] LeaveTransactionDto dto)
            => _leaveTransactionService.Create(dto);
    }
}