﻿using System.Collections.Generic;
using HRIS.Application.IServices;
using HRIS.Common.Dtos;
using HRIS.Common.Responses;
using Microsoft.AspNetCore.Mvc;

namespace HRIS.API.Controllers
{
    [Route("api/leave/type")]
    [ApiController]
    public class LeaveTypeController : ControllerBase
    {
        private readonly ILeaveTypeService _leaveTypeService;

        public LeaveTypeController(ILeaveTypeService leaveTypeService)
            => _leaveTypeService = leaveTypeService;

        [HttpGet("list")]
        public BaseResponse<IEnumerable<LeaveTypeDto>> LeaveTypes()
            => _leaveTypeService.GetAll();

        [HttpPost("create")]
        public BaseResponse Create([FromBody] LeaveTypeDto dto)
            => _leaveTypeService.Create(dto);

        [HttpPut("update")]
        public BaseResponse Update([FromBody] LeaveTypeDto dto)
            => _leaveTypeService.Update(dto);

        [HttpDelete("delete")]
        public BaseResponse Update([FromBody] string id)
            => _leaveTypeService.Delete(id);
    }
}