﻿using HRIS.Common.Constants;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HRIS.Common.Responses
{
    public class BaseResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseCode Code { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }
    }

    public class BaseResponse<T>
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseCode Code { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        public T Data { get; set; }
    }
}
