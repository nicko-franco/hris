﻿namespace HRIS.Common.Responses
{
    public class LoginResponse
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
}