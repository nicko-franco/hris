﻿using System.ComponentModel;

namespace HRIS.Common.Constants
{
    public enum ResponseCode
    {
        Success,
        Failed,
        UnexpectedError,
        DuplicateRecord,
        RecordNotFound
    }

    public enum AuthenticationMessage
    {
        [Description("Username does not exist.")]
        UsernameDoesNotExists,

        [Description("Invalid password. Please try again.")]
        InvalidPassword
    }

    public enum GenericError
    {
        [Description("{0} already exists.")]
        DuplicateRecord,

        [Description("{0} does not exist.")]
        RecordNotFound
    }
}
