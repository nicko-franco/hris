﻿namespace HRIS.Common.Constants
{
    public static class SecurityConstants
    {
        public const string Hash = "ITAS.HRIS";
    }

    public enum Cryptography
    {
        Encrypt,
        Decrypt
    }
}