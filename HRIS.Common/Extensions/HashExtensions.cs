﻿using HRIS.Common.Constants;
using System.Security.Cryptography;
using System.Text;

namespace HRIS.Common.Extensions
{
    public static class HashExtensions
    {
        public static string Hash(this string password, Cryptography cryptography)
        {
            byte[] data = Encoding.UTF8.GetBytes(password);

            using (var md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(Encoding.UTF8.GetBytes(SecurityConstants.Hash));

                using (var tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    var transform = cryptography.Equals(Cryptography.Encrypt) ? tripDes.CreateEncryptor() : tripDes.CreateDecryptor();

                    return Encoding.UTF8.GetString(transform.TransformFinalBlock(data, 0, data.Length));
                }
            }
        }
    }
}
