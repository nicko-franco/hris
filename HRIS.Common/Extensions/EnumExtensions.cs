﻿using System;
using System.ComponentModel;

namespace HRIS.Common.Extensions
{
    public static class EnumExtensions
    {
        public static string Description(this Enum enums)
        {
            var attributes = (DescriptionAttribute[])enums
                .GetType()
                .GetField(enums.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
