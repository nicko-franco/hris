﻿using HRIS.Common.Responses;

namespace HRIS.Common.Extensions
{
    public static class BaseResponseExtensions
    {
        public static BaseResponse Base<T>(this BaseResponse<T> baseResponse)
            => new BaseResponse { Code = baseResponse.Code, Message = baseResponse.Message };
    }
}
