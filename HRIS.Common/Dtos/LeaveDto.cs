﻿using System;

namespace HRIS.Common.Dtos
{
    public class LeaveDto
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeNumber { get; set; }
        public string LeaveTypeId { get; set; }
        public decimal Balance { get; set; }
        public int Year { get; set; }
    }

    public class LeaveBalanceDto
    {
        public int Year { get; set; }
        public decimal Balance { get; set; }
        public string LeaveType { get; set; }
    }

    public class LeaveTypeDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class LeaveTransactionDto
    {
        public string Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeId { get; set; }

        public string LeaveType { get; set; }
        public string LeaveTypeId { get; set; }

        public string Status { get; set; }
        public string Reason { get; set; }
        public string DeclinedReason { get; set; }

        public decimal Amount { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime AppliedDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
    }
}
