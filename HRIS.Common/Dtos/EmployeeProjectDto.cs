﻿namespace HRIS.Common.Dtos
{
    public class EmployeeProjectDto
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
    }
}
