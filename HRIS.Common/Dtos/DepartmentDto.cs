﻿namespace HRIS.Common.Dtos
{
    public class DepartmentDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Head { get; set; }
    }
}
