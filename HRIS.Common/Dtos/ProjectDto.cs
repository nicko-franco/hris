﻿namespace HRIS.Common.Dtos
{
    public class ProjectDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
