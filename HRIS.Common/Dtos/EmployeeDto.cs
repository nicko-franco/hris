﻿using System;

namespace HRIS.Common.Dtos
{
    public class EmployeeDto
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Password { get; set; }
        public string DepartmentId { get; set; }
        public string Nationality { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNumber { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
}
